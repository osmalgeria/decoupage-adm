# OpenStreetMap Algeria

Ici on héberge quelques outils accessibles via [https://osmalgeria.gitlab.io](https://osmalgeria.gitlab.io)

* Outil pour le suivi des [niveaux administratifs](https://osmalgeria.gitlab.io/collectivites/) des collectivités.

### Forum

[Le forum](https://forum.openstreetmap.org/viewforum.php?id=53) de discussions OSMAlgeria
