"use strict";
const fs = require("fs");
let newCommunes = [];
fs.readFile("./communeCode.json", (err, data) => {
  if (err) throw err;
  let communes = JSON.parse(data);
  // console.log(communes);
  let wKeys = Object.keys(communes);
  //array1.forEach(element => console.log(element));

  wKeys.forEach((el) => {
    let cKeys = Object.keys(communes[el]["communes"]);
    let wilaya = communes[el]["communes"];
    cKeys.forEach((cEl) => {
      let currentCommune = communes[el]["communes"][cEl];
      let wref = getWilayaRef(wilaya[cEl]["ref-ons"]);
      currentCommune["wilaya-ref"] = wref;
      newCommunes.push(currentCommune);
    });
  });
  console.log(newCommunes);

  let dataOut = JSON.stringify(newCommunes);
  fs.writeFileSync("./newcommuneCode.json", dataOut);
});

function getWilayaRef(ref) {
  if (ref.length == 3) return "0" + ref[0];
  return ref.slice(0, 2);
}
