let Site_Config = {
  Page_Title: "Découpage administratif",

  Nominatim_Url: "https://nominatim.openstreetmap.org/details.php",
  overpassUrls: [
    "https://overpass-api.de/api/interpreter?data=",
    "http://overpass.openstreetmap.ru/cgi/interpreter?data=",
    "https://overpass.kumi.systems/api/interpreter?data=",
    "//overpass.openstreetmap.fr/api/interpreter?data=",
  ],

  // ---- MAP ----
  Reverse_Default_Search_Zoom: 18,
  Map_Default_Lat: 20.0,
  Map_Default_Lon: 0.0,
  Map_Default_Zoom: 2,

  //https://maps.radioalgerie.dz/osm_tiles2/7/68/52.png
  Map_Tile_URL: {
    ar: "https://maps.radioalgerie.dz/osm_tiles2/{z}/{x}/{y}.png",
    fr: "https://maps.radioalgerie.dz/fr_tiles/{z}/{x}/{y}.png",
    ber: "https://maps.radioalgerie.dz/osm_tiles4/{z}/{x}/{y}.png",
    en: "https://{s}.tile.osm.org/{z}/{x}/{y}.png",
    kab: "https://maps.radioalgerie.dz/osm_tiles3/{z}/{x}/{y}.png",
  },

  Map_Tile_Attribution:
    '<a href="https://osm.org/copyright">OpenStreetMap contributors</a>',
};
