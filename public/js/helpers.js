"use strict";
dayjs.locale("fr");
dayjs.extend(window.dayjs_plugin_relativeTime);

let wilayaList = [];
let dairaCode = {};
var jsonFileUrl = "./data/wilayas.json";
var jsonDairaUrl = "./data/code-dairas.json";
var dairaInfoUrl = "./data/dairaCode.json";
var communeInfoUrl = "./data/codeCommunes.json"; ///home/khaled/code/authorities/public/data/codeCommunes.json
let dairaInfodata = [];
var communeInfoData = {};

fetch_from_api(jsonFileUrl, (data, error) => {
  if (!error) return (wilayaList = data);
  console.log(error);
});

fetch_from_api(dairaInfoUrl, (data, error) => {
  // console.log(data);
  if (!error) return (dairaInfodata = data);

  console.log(error);
});

fetch_from_api(communeInfoUrl, (data, error) => {
  if (!error) return (communeInfoData = data);
  console.log(error);
});

var apiErrors = [];
/**
 *
 * @param {string} api_url formated uri
 * @param {callback} callback
 * @returns {Promise}
 */

async function fetch_from_api(api_url, callback) {
  try {
    apiErrors = [];
    const rawreponse = await fetch(api_url);
    const jsonResponse = await rawreponse.json();
    // console.log(api_url, jsonResponse);
    callback(jsonResponse);
  } catch (error) {
    callback(error, true);
    // return `Error fetching data from ${api_url} (${error})`;
  }
}

function getWilayaByRef(ref, wilayas) {
  //   console.log("wilayas", wilayas);
  //   console.log("ref", typeof ref, ref);
  const wilaya = wilayas.filter((item) => ref == item.ref);
  //   console.log("wilaya : ", wilaya);
  return wilaya[0];
}

String.prototype.sansAccent = function () {
  var accent = [
    /[\300-\306]/g,
    /[\340-\346]/g, // A, a
    /[\310-\313]/g,
    /[\350-\353]/g, // E, e
    /[\314-\317]/g,
    /[\354-\357]/g, // I, i
    /[\322-\330]/g,
    /[\362-\370]/g, // O, o
    /[\331-\334]/g,
    /[\371-\374]/g, // U, u
    /[\321]/g,
    /[\361]/g, // N, n
    /[\307]/g,
    /[\347]/g, // C, c
  ];
  var noaccent = [
    "A",
    "a",
    "E",
    "e",
    "I",
    "i",
    "O",
    "o",
    "U",
    "u",
    "N",
    "n",
    "C",
    "c",
  ];

  var str = this;
  for (var i = 0; i < accent.length; i++) {
    str = str.replace(accent[i], noaccent[i]);
  }

  return str;
};

var FX = {
  easing: {
    linear: function (progress) {
      return progress;
    },
    quadratic: function (progress) {
      return Math.pow(progress, 2);
    },
    swing: function (progress) {
      return 0.5 - Math.cos(progress * Math.PI) / 2;
    },
    circ: function (progress) {
      return 1 - Math.sin(Math.acos(progress));
    },
    back: function (progress, x) {
      return Math.pow(progress, 2) * ((x + 1) * progress - x);
    },
    bounce: function (progress) {
      for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
        if (progress >= (7 - 4 * a) / 11) {
          return (
            -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2)
          );
        }
      }
    },
    elastic: function (progress, x) {
      return (
        Math.pow(2, 10 * (progress - 1)) *
        Math.cos(((20 * Math.PI * x) / 3) * progress)
      );
    },
  },
  animate: function (options) {
    var start = new Date();
    var id = setInterval(function () {
      var timePassed = new Date() - start;
      var progress = timePassed / options.duration;
      if (progress > 1) {
        progress = 1;
      }
      options.progress = progress;
      var delta = options.delta(progress);
      options.step(delta);
      if (progress == 1) {
        clearInterval(id);
        options.complete();
      }
    }, options.delay || 10);
  },
  fadeOut: function (element, options) {
    var to = 1;
    this.animate({
      duration: options.duration,
      delta: function (progress) {
        progress = this.progress;
        return FX.easing.swing(progress);
      },
      complete: options.complete,
      step: function (delta) {
        element.style.opacity = to - delta;
      },
    });
  },
  fadeIn: function (element, options) {
    var to = 0;
    this.animate({
      duration: options.duration,
      delta: function (progress) {
        progress = this.progress;
        return FX.easing.swing(progress);
      },
      complete: options.complete,
      step: function (delta) {
        element.style.opacity = to + delta;
      },
    });
  },
};

function getInterieurLinkByRef(ref, wilayas = []) {
  if (!ref || wilayas.length == 0) return false;
  let wilayaCode = ref.substr(0, 2);
  let communeCode = ref.substr(2, 2);
  let wilaya = wilayas.filter((item) => item.ref == wilayaCode)[0];
  return parseInt(wilaya.indice) + parseInt(communeCode);
}

function getDetails(ref, code, admin_level, name) {
  let urlFirstPart =
    "https://interieur.gov.dz/index.php/fr/component/annuaires/";
  let urlLastPart = ".html";
  let info;
  let url;
  switch (admin_level) {
    case "8":
      info = getCommuneByRef(code, communeInfoData);
      url = urlFirstPart + "annuairecommune/" + info?.ref + urlLastPart;
      break;
    case "6":
      info = dairaByName(name, ref, dairaInfodata);
      url = urlFirstPart + "annuairedaira/" + info.ref + urlLastPart;
      break;
  }
  // console.log("info", info);
  return { text: info, url };
}
function getCommuneByRef(ref, wilayaData) {
  // console.log("wilayaData", wilayaData);
  return wilayaData.filter((item) => {
    // console.log("item", item);
    return item["ref-ons"] == ref;
  })[0];
}
String.prototype.capitalizeNames = function () {
  let newStr = [];
  var str = this;
  var lowerstr = str.toLowerCase().split(" ");
  for (word of lowerstr) {
    newStr.push(word.charAt(0).toUpperCase() + word.slice(1));
  }
  return newStr.join(" ");
};

function wikilink(wikipedia) {
  if (wikipedia == undefined) {
    return "";
  }
  let lang = wikipedia.substr(0, 2);
  let raw = wikipedia.substr(3);
  //https://fr.wikipedia.org/wiki/Ouled_Djellal
  return /*html*/ `<a href="https://${lang}.wikipedia.org/wiki/${encodeURI(
    raw
  )}" target="_blank" title="Wikipedia"><button class="icon"><img class="icon" src="images/Wikipedia-globe-icon.png"></button></a>`;
  // <a href="http://osmrm.openstreetmap.de/relation.jsp?id=${id}" target="_blank" title="Relation Manager"><button class="icon">Ma</button></a>
}

function editLink(index, row, element) {
  let id = row.id;
  let wikipedia = row.tags.wikipedia;
  return /*html*/ `<a href="https://www.openstreetmap.org/relation/${id}" target="_blank" title="OpenStreetMap"> <button class="icon"><img class="icon" src="images/osm-logo.png"></button></a>&nbsp;
<a href="http://127.0.0.1:8111/load_object?new_layer=false&amp;relation_members=true&amp;objects=relation${id}" target="hiddenIframe" title="JOSM editor"><button class="icon"><img class="icon" src="images/josm-logo.png"></button></a>&nbsp;
<a href="https://www.openstreetmap.org/edit?editor=id&amp;relation=${id}" target="_blank" title="ID editor"><button class="icon"><img class="icon" src="images/id-logo.png"></button></a>&nbsp;
<a href="http://level0.osmz.ru/?url=relation/${id}" target="_blank" title="Level0 editor"><button class="icon">L0</button></a>&nbsp;
<a href="http://ra.osmsurround.org/analyzeRelation?relationId=${id}" target="_blank" title="Relation Analyzer"><button class="icon">An</button></a>&nbsp;
<a href="http://osmrm.openstreetmap.de/relation.jsp?id=${id}" target="_blank" title="Relation Manager"><button class="icon">Ma</button></a>&nbsp;
${wikilink(wikipedia)}`;
}

const localstringSimilarity = function (
  str1,
  str2,
  substringLength,
  caseSensitive
) {
  if (substringLength === void 0) {
    substringLength = 2;
  }
  if (caseSensitive === void 0) {
    caseSensitive = false;
  }
  if (!caseSensitive) {
    str1 = str1.toLowerCase();
    str2 = str2.toLowerCase();
  }
  str1 = str1.replace("ï", "i");
  str2 = str2.replace("ï", "i");
  str1 = str1.replace("é", "e");
  str2 = str2.replace("é", "e");

  if (str1.length < substringLength || str2.length < substringLength) return 0;
  var map = new Map();
  for (var i = 0; i < str1.length - (substringLength - 1); i++) {
    var substr1 = str1.substr(i, substringLength);
    map.set(substr1, map.has(substr1) ? map.get(substr1) + 1 : 1);
  }
  var match = 0;
  for (var j = 0; j < str2.length - (substringLength - 1); j++) {
    var substr2 = str2.substr(j, substringLength);
    var count = map.has(substr2) ? map.get(substr2) : 0;
    if (count > 0) {
      map.set(substr2, count - 1);
      match++;
    }
  }
  return (match * 2) / (str1.length + str2.length - (substringLength - 1) * 2);
};

/**
 *
 * @param {Array} array array of object
 * @param {String} test
 * @param {Integer} minRatio
 * @returns {Object} return single object
 */
function bestMatch(array, key, test, minRatio = 0.51) {
  let intermediareArray = [];
  // cas exceptionnel abalessa = silet
  test = test.toLowerCase().sansAccent();
  test = test == "Abalessa" ? "Silet" : test;
  // console.log("params: ", array, key, test);
  array.map((item) => {
    // console.log(item[key]);
    if (
      localstringSimilarity(item[key].toLowerCase().sansAccent(), test) >=
      minRatio
    )
      intermediareArray.push({
        ...item,
        cost: localstringSimilarity(item[key].toLowerCase().sansAccent(), test),
      });
  });
  // console.log("intermediareArray: befor", intermediareArray);
  intermediareArray.sort((a, b) => b.cost - a.cost);
  // console.log("intermediareArray: after", intermediareArray);
  return intermediareArray[0];
}

function generate_nominatim_api_url(params) {
  return (
    Site_Config.Nominatim_Url +
    "?" +
    Object.keys(clean_up_parameters(params))
      .map((k) => {
        return encodeURIComponent(k) + "=" + encodeURIComponent(params[k]);
      })
      .join("&")
  );
}

function clean_up_parameters(params) {
  var param_names = Object.keys(params);
  for (var i = 0; i < param_names.length; i += 1) {
    var val = params[param_names[i]];
    if (typeof val === "undefined" || val === "" || val === null) {
      delete params[param_names[i]];
    }
  }
  return params;
}

async function wikiSummary(wikiPage, element) {
  const wikiSummaryDiv = document.getElementById(element);
  wikiSummaryDiv.innerHTML = "";

  let [lang, page] = wikiPage.split(":");
  // console.log("page:", page);
  const url = encodeURI(
    `https://${lang}.wikipedia.org/api/rest_v1/page/summary/` +
      page +
      "?uselang=" +
      lang
  );
  // console.log("url", url);
  fetch_from_api(url, (data) => {
    // console.log(data);
    if (data.title == "Not found." || data.type == "no-extract")
      return (wikiSummaryDiv.innerHTML = "Not found.");
    wikiSummaryDiv.innerHTML = /*html*/ `
   <a class="mwe-popups-discreet" href="https://${data.lang}.wikipedia.org/wiki/${data.title}"><img class="mwe-popups-thumbnail" src="${data.thumbnail?.source}"></a>
    <a class="mwe-popups-extract" href="https://${data.lang}.wikipedia.org/wiki/${data.title}" dir="${data.dir}" lang="${data.lang}" target="_blank">${data.extract_html}</a>
    `;
  });
}

function dairaByName(name, wilayaRef, data) {
  // console.log("wilayaList", data);
  // if (dairaInfodata.length == 0) return false;
  // var dairas = this.daira;
  var wilaya = data.filter((item) => item["ref-wilaya"] == wilayaRef);

  let test = bestMatch(wilaya, "name", name);
  // console.log("test ", test);

  return test;
}
// console.log(dairaInfodata);
