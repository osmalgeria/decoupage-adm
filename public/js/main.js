const tableTitle = document.getElementById("table-title");
const loaderDiv = document.getElementById("loader");
const wilayaDetailsDiv = document.getElementById("wilaya-details");
const wilayaSelector = document.getElementById("wilaya");
const goButton = document.getElementById("go-button");
const $communeTable = $("#communes-table");
const $dairaTable = $("#daira-table");
let wilayasData = {};

let storeDetails = {};
let nominatimStore = {};
let map = {};
document.title = Site_Config.Page_Title;
var form = document.getElementById("myForm");
let url = Site_Config.overpassUrls[2];

$("#wilaya").select2({
  width: "resolve",
  language: {
    errorLoading: function () {
      return "Les résultats ne peuvent pas être chargés.";
    },
    inputTooLong: function (args) {
      var overChars = args.input.length - args.maximum;

      return (
        "Supprimez " + overChars + " caractère" + (overChars > 1 ? "s" : "")
      );
    },
    inputTooShort: function (args) {
      var remainingChars = args.minimum - args.input.length;

      return (
        "Saisissez au moins " +
        remainingChars +
        " caractère" +
        (remainingChars > 1 ? "s" : "")
      );
    },
    loadingMore: function () {
      return "Chargement de résultats supplémentaires…";
    },
    maximumSelected: function (args) {
      return (
        "Vous pouvez seulement sélectionner " +
        args.maximum +
        " élément" +
        (args.maximum > 1 ? "s" : "")
      );
    },
    noResults: function () {
      return "Aucun résultat trouvé";
    },
    searching: function () {
      return "Recherche en cours…";
    },
    removeAllItems: function () {
      return "Supprimer tous les éléments";
    },
    removeItem: function () {
      return "Supprimer l'élément";
    },
  },
});
$("#wilaya").on("select2:select", function (event) {
  if (event.target.value != "") {
    goButton.disabled = false;
  } else goButton.disabled = true;
});

form.addEventListener("submit", function (event) {
  event.preventDefault();
  var wilayaSelected = document.getElementById("wilaya").value;
  var admLevel = document.getElementById("level").value;
  FX.fadeOut(document.getElementById("main"), {
    duration: 500,
    complete: function () {},
  });
  document.getElementById("loader").style.display = "block";
  fetchData(admLevel, wilayaSelected);
});

//function loadWilayasList() {}

function loadWilayaFromOverpassApi(level, ref) {
  if (wilayasData[level] && wilayasData[level][ref])
    return setTimeout(() => {
      buildTable(level, ref);
    }, 1000);
  wilwilayasData = { ...wilayasData, level };
  let name_fr = getWilayaByRef(ref, wilayaList).name_fr;
  let name = name_fr
    .replace(/([0-9]+:\s+)/g, "")
    .replace(/\s+/g, "_")
    .replace(/'/g, "")
    .sansAccent()
    .toLowerCase();
  if (ref == "34") name = "bba";
  let areacode = getWilayaByRef(ref, wilayaList).rel_area;
  let query = queryBuilder(level, areacode, name);
  fetch_from_api(url + query, (data) => {
    wilayasData[level] = { ...wilayasData[level], ref };
    wilayasData[level][ref] = data.elements;

    return buildTable(level, ref);
  });
}

function fetchData(level, wilayaRef) {
  if (!level || !wilayaRef) return false;
  loaderDiv.style.display = "block";
  const wilaya = getWilayaByRef(wilayaRef, wilayaList);
  if (level == "4") {
    loadWilaya(wilayaRef);
  } else {
    loadWilayaFromOverpassApi(level, wilayaRef);
  }
}
function loadData(level, ref) {
  loadWilayaFromOverpassApi(level, ref);
}

async function loadWilaya(ref) {
  const wilaya = getWilayaByRef(ref, wilayaList);
  const osmid = wilaya.id;

  const params = {
    osmtype: "R",
    osmid,
    addressdetails: 1,
    hierarchy: 0,
    group_hierarchy: 1,
    polygon_geojson: 1,
    format: "json",
  };

  const url = generate_nominatim_api_url(params);
  if (nominatimStore[osmid]) return buildWilayaDiv(ref, nominatimStore[osmid]);
  fetch_from_api(url, (data) => {
    buildWilayaDiv(ref, data);

    nominatimStore[osmid] = data;
  });
}

function buildWilayaDiv(ref, thisWilaya) {
  const wilaya = getWilayaByRef(ref, wilayaList);
  document.title =
    Site_Config.Page_Title + " | Infos sur la wailaya de: " + wilaya.name_fr;
  FX.fadeIn(document.getElementById("main"), {
    duration: 500,
    complete: function () {},
  });
  console.log("thiswilaya", thisWilaya);
  //<li>${thisWilaya.names["name:fr"]}</li>
  reseteveryThing();
  const wikiPage = thisWilaya.extratags?.wikipedia;
  const wikiElement = "wiki" + thisWilaya.osm_id;
  const wikiContent = !wikiPage
    ? ""
    : /*html*/ `
    <div class="details">
      <div class="mwe-popups mwe-popups-type-page mwe-popups-fade-in-up mwe-popups-no-image-pointer mwe-popups-is-tall">
          <div class="mwe-popups-container" id="${wikiElement}"> </div>
      </div>
    </div>`;

  loaderDiv.style.display = "none";
  wilayaDetailsDiv.innerHTML = /*html*/ `
    <div class="details-container">
      <div class="details">
        ${wikiContent}
      </div>
      <div class="details" id="${ref}">
      Map
      </div>
    </div>
  `;
  if (wikiPage) wikiSummary(wikiPage, wikiElement);
  createMap(parse_and_normalize_geojson_string(thisWilaya.geometry), ref, ref);
}

function queryBuilder(level, areacode, name) {
  return `[out:json][timeout:300];
area(${areacode})->.${name};
(
  relation
    ["admin_level"="${level}"](area.${name});
);
out meta center;
>;
out count;`;
}

function buildTable(level, ref) {
  switch (level) {
    case "8":
      buildCommuneTable(wilayasData[level][ref], ref);
      loaderDiv.style.display = "none";
      document.title =
        Site_Config.Page_Title +
        " | liste des communes de: " +
        getWilayaByRef(ref, wilayaList).name_fr;
      break;
    case "6":
      buildDairaTable(wilayasData[level][ref], ref);
      loaderDiv.style.display = "none";
      document.title =
        Site_Config.Page_Title +
        " | liste des daïras de: " +
        getWilayaByRef(ref, wilayaList).name_fr;
      break;

    default:
      break;
  }
  FX.fadeIn(document.getElementById("main"), {
    duration: 500,
    complete: function () {},
  });
}
function buildCommuneTable(data, ref) {
  data.pop();
  reseteveryThing();
  tableTitle.innerHTML =
    "Liste des communes de la wilaya de: " +
    getWilayaByRef(ref, wilayaList).name_fr;
  $communeTable.bootstrapTable({
    data: data,
    pagination: data.length >= 10,
    pageSize: 20,
    search: data.length >= 10,
    mobileResponsive: true,
    undefinedText: "N/A",
    showFooter: true,
    locale: "fr-FR",
    detailFormatter: "detailFormatter",
    detailView: true,
    detailViewByClick: false,
    detailViewIcon: true,
    showRefresh: true,

    columns: [
      {
        field: "tags.name",
        title: "Name",
        formatter: "nameFormatter",
      },
      {
        field: "tags.name:fr",
        title: "Français",
        sortable: true,
      },
      {
        field: "tags.name:ar",
        title: "Arabe",
        sortable: true,
      },
      {
        field: "tags.name:ber",
        title: "Tifinagh",
        sortable: true,
      },
      {
        field: "tags.name:kab",
        title: "Kabyle",
        sortable: true,
      },
      {
        field: "tags.ref",
        title: "Ref:ONS",
        sortable: true,
      },
      {
        field: "tags.population",
        title: "Population",
        titleTooltip: "Office National des Statistiques RGPH 2008",
        sortable: true,
        footerFormatter: "totalPopulation",
      },
      {
        field: "user",
        title: "Utilisateur",
        formatter: "userFormatter",
      },
      {
        field: "timestamp",
        title: "Dernière modif.",
        formatter: "dateFormatter",
      },
      {
        field: "id",
        title: "Info | Edition",
        formatter: "editLink",
      },
    ],
  });
}
function buildDairaTable(data, ref) {
  reseteveryThing();
  tableTitle.innerHTML =
    "Liste des daïras de la wilaya de: " +
    getWilayaByRef(ref, wilayaList).name_fr;
  $dairaTable.bootstrapTable({
    data: data.slice(0, -1),
    pagination: data.length >= 10,
    search: data.length >= 10,
    mobileResponsive: true,
    locale: "fr-FR",
    detailFormatter: "detailFormatter",
    detailView: true,
    detailViewByClick: false,
    detailViewIcon: true,
    showRefresh: true,
    columns: [
      {
        field: "tags.name",
        title: "Name",
        formatter: "nameFormatter",
      },
      {
        field: "tags.name:fr",
        title: "Name Fr",
        sortable: true,
      },
      {
        field: "tags.name:ar",
        title: "Name ar",
        sortable: true,
      },
      {
        field: "tags.name:ber",
        title: "Name ber",
        sortable: true,
      },
      {
        field: "tags.name:kab",
        title: "Name kab",
        sortable: true,
      },
      {
        field: "user",
        title: "Utilisateur",
        formatter: "userFormatter",
      },
      {
        field: "timestamp",
        title: "Dernière modif.",
        formatter: "dateFormatter",
      },
      {
        field: "id",
        title: "Info | Edition",
        formatter: "editLink",
      },
    ],
  });
  FX.fadeIn(document.getElementById("main"), {
    duration: 1000,
    complete: function () {},
  });
}

function osmLink(value) {
  return /*html*/ `<a href="https://openstreetmap.org/relation/${value}" target="_blank">${value} </a>`;
}

function totalPopulation(value) {
  return value
    .map(function (row) {
      number = row["tags"]["population"];
      number = number != undefined ? parseInt(number) : 0;
      return +number;
    })
    .reduce(function (sum, i) {
      return sum + i;
    }, 0);
}

function reseteveryThing() {
  $communeTable.bootstrapTable("destroy");
  $dairaTable.bootstrapTable("destroy");
  wilayaDetailsDiv.innerHTML = tableTitle.innerHTML = $communeTable.innerHTML = $dairaTable.innerHTML =
    "";
}

function dateFormatter(value) {
  let fromNow = dayjs(value).fromNow();
  let lastEdit = dayjs(value).format("DD/MM/YYYY");
  return /*html*/ `<span title="${lastEdit}">${fromNow} </span>`;
}

function getDairaCode(ref, name) {
  // console.log("ref", ref);
  // console.log(name.replace("Daïra ", ""));
  let wilaya = dairaCode[ref];
  nameFromOsm = name.replace(/da(?:i|ï)ra\s(d(?:'|e))?/gi, "").trim();

  let match = bestMatch(wilaya, "name", nameFromOsm, 0.5);
  // console.log("match:", match);
  return match.code ?? undefined; // test[0]?.code ?? null; //test[0].code;
}

async function detailFormatter(index, row, element) {
  // wikiSummary(row.tags.wikipedia);
  const wikiPage = row.tags?.wikipedia;
  const wikiElement = "wiki" + row.id;
  const wikiContent = !wikiPage
    ? ""
    : /*html*/ `
    <div class="details">
      <div class="mwe-popups mwe-popups-type-page mwe-popups-fade-in-up mwe-popups-no-image-pointer mwe-popups-is-tall">
          <div class="mwe-popups-container" id="${wikiElement}"> </div>
      </div>
    </div>`;
  let admin_level = row.tags.admin_level;
  let ref = row.tags.ref;
  let name = row.tags["name:fr"];
  // console.log("row.tags", row.tags);
  let refWilaya =
    parseInt($("#wilaya").val()) > 48
      ? getWilayaByRef($("#wilaya").val(), wilayaList).old_ref
      : $("#wilaya").val();
  console.log("variables:", refWilaya, ref, admin_level, '"' + name + '"');
  let { text, url } = getDetails(refWilaya, ref, admin_level, name);
  console.log("text:", text);
  if (!text || text == undefined) {
    text = {
      name: "Not found",
      daira: "N/A",
      contact: {
        fax: [],
        phones: [],
        email: "",
        website: "",
        address: "",
      },
    };
    url =
      "https://interieur.gov.dz/index.php/fr/component/annuaires/annuairecommunes.html";
  }
  let interieurName = text.name;
  // console.log("text", text);
  let { address, fax, phones } = text.contact;
  let email = text.contact["e-mail"];
  let website = text.contact.website
    ? `Site internet:   <a href="${text.contact.website}">${text.contact.website}</a><br />`
    : "";
  let daira = admin_level == "8" ? "Daïra: " + text.daira : "";
  let diff = localstringSimilarity(interieurName, row.tags["name:fr"]);

  let mapId = ref + "-" + randBetween(1, 99);
  // console.log(mapId);
  let color =
    diff >= 0.5 || interieurName == "Djasr Kasentina" ? "green" : "red";
  element[0].innerHTML = /*html*/ `
  <div class="details-container">
    <div class="details">
        <div class="card border-success mb-3" >
          <div class="card-header bg-transparent border-success">Contact infos</div>
            <div class="card-body text-success">
              <h5 class="card-title">OSM:<span class="green">${row.tags["name:fr"]}</span>&nbsp;&nbsp;|&nbsp;&nbsp;MICL: <span style="color: ${color} !important;">${interieurName}</span> </h5>
              <p class="card-text bg-transparent">
                <i class="fas fa-phone"></i> : ${phones[0]}<br />
                <i class="fas fa-fax"></i> : ${fax[0]}<br />
                <i class="fas fa-at"></i> : ${email}<br />
                <i class="far fa-envelope"></i></i> : ${address}<br />
                ${website}
                ${daira}
              </p>
            </div>
            <div class="card-footer bg-transparent border-success">Source: <a href="${url}" target="_blank">Interieur.gov.dz</a> </div>
        
      </div></div>
      ${wikiContent}
  <div class="details"><div class="map" id="${mapId}" ></div>
  </div>
  </div>
  `;
  if (wikiPage) wikiSummary(wikiPage, wikiElement);
  getNominatimData(ref, row.id, mapId);
}
function nameFormatter(value) {
  return value
    .replace(/[a-zàâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇ]+/gi, (match) => {
      return `<span class="text-warning">${match} </span> `;
    })
    .replace(/[\u0600-\u06FF]+/g, (match) => {
      return `<span class="text-info">${match} </span> `;
    });
}

async function getNominatimData(ref, id, mapId) {
  const nominatimUrl = id
    ? `https://nominatim.openstreetmap.org/details.php?osmtype=R&osmid=${id}&addressdetails=1&hierarchy=0&group_hierarchy=1&polygon_geojson=1&format=json`
    : undefined;
  if (!nominatimUrl) return;
  if (nominatimStore[id])
    return createMap(
      parse_and_normalize_geojson_string(nominatimStore[id].geometry),
      ref,
      mapId
    );

  await fetch_from_api(nominatimUrl, (data) => {
    nominatimStore[id] = data && !data.error ? data : undefined;
  });

  createMap(
    parse_and_normalize_geojson_string(nominatimStore[id].geometry),
    ref,
    mapId
  );
}

function createMap(nominatimGeoJson, element, mapId) {
  let map = null;
  if (map) map.remove();
  map = L.map(mapId);
  var frMap = new L.TileLayer(Site_Config.Map_Tile_URL.en, {
    maxZoom: 19,
    attribution: Site_Config.Map_Tile_Attribution,
  });
  map.addLayer(frMap);
  var geojson_layer = L.geoJson(nominatimGeoJson, {
    style: function () {
      return { interactive: false, color: "blue" };
    },
  });
  map.addLayer(geojson_layer);
  map.fitBounds(geojson_layer.getBounds());
}

function parse_and_normalize_geojson_string(part) {
  var parsed_geojson = {
    type: "FeatureCollection",
    features: [
      {
        type: "Feature",
        geometry: part,
        properties: {},
      },
    ],
  };
  return parsed_geojson;
}

function randBetween(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function userFormatter(value) {
  return /*html*/ `<a href="https://www.openstreetmap.org/user/${encodeURI(
    value
  )}"  target="_blank">${value}</a>`;
}
